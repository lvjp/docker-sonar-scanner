# docker-sonar-scanner

[![GitLab CI](https://gitlab.com/lvjp/docker-sonar-scanner/badges/master/pipeline.svg)](https://gitlab.com/lvjp/docker-sonar-scanner/commits/master)
[![Licence](https://img.shields.io/badge/license-GPL--3.0-brightgreen)](https://gitlab.com/lvjp/docker-sonar-scanner/blob/master/COPYING.md)

This project package the
[sonar-scanner](https://sonarcloud.io/documentation/analysis/scan/sonarscanner/) inside a
docker image ready to be used inside a GitLab CI pipeline.

## License

Full license can be found in the [COPYING.md](COPYING.md) file.

        Copyright (C) 2019 VERDOÏA Laurent

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
