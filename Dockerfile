# vim:set ts=4 sw=4 tw=95 et:

FROM ubuntu:bionic-20191029 AS builder

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --yes \
        wget \
        unzip \
    && wget -O /tmp/sonar-scanner.zip \
        https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.2.0.1873-linux.zip \
    && unzip /tmp/sonar-scanner.zip -d /opt

FROM ubuntu:bionic-20191029

# Install git allow user to fetch sonar branch merge target ref.
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install \
        --yes --no-install-recommends \
        ca-certificates \
        git

COPY --from=builder /opt/sonar-scanner-4.2.0.1873-linux /opt/sonar-scanner-4.2.0.1873-linux
ENV PATH=/opt/sonar-scanner-4.2.0.1873-linux/bin:$PATH

ENTRYPOINT [ "/opt/sonar-scanner-4.2.0.1873-linux/bin/sonar-scanner" ]
